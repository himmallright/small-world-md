check:
	black --check --diff .
lint:
	black .
dev-install:
	pip install --editable .
test:
	pytest -v .