#!/usr/bin/env python
"""setuptools script for installing small-world-md"""
import os
from setuptools import find_packages, setup

_project_root = os.path.abspath(os.path.dirname(__file__))

setup(
    name="small_world_md",
    version="1.0.0",
    author="Ryan Himmelwright",
    author_email="ryan.himmelwright@gmail.com",
    description="Parses an Obsidian note vault, and runs network graph calculations on it",
    license="GPL-3.0",
    keywords=["notes", "graphs", "obsidian"],
    packages=find_packages(include=["small_world_md*"]),
    scripts=["bin/small-world-md"],
)
