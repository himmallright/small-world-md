from constants import TEST_MD_LINKS_DATA, EXPECTED_EDGE_COUNT, EXPECTED_NODE_COUNT
from pkg_resources import resource_filename
from small_world_md.graph import *
from utils import compare_lists, compare_dicts

import pytest


def test_get_all_nodes_set():
    expected_result = [
        "node F",
        "node a",
        "node d",
        "node e (template)",
        "node-b",
        "node_c",
    ]
    returned_result = get_all_nodes_set(TEST_MD_LINKS_DATA)
    assert compare_lists(returned_result, expected_result)


def test_create_node_index():
    expected_result = {
        "node a": 1,
        "node e (template)": 3,
        "node_c": 5,
        "node F": 0,
        "node-b": 4,
        "node d": 2,
    }
    returned_result = create_node_index(TEST_MD_LINKS_DATA)
    compare_dicts(returned_result, expected_result)


def test_build_graph(test_graph):
    expected_result = "IGRAPH U--- 6 8 --\n+ edges:\n0 -- 1             2 -- 1 4 5         4 -- 1 2 5\n1 -- 0 1 1 2 4 5   3 --               5 -- 1 2 4"
    # result_graph = build_graph(TEST_MD_LINKS_DATA)
    assert str(test_graph) == expected_result


def test_build_er_graph(node_count=EXPECTED_NODE_COUNT, edge_count=EXPECTED_EDGE_COUNT):
    """Tests the ER graph is built. This is random, so just checking it is \
    built with correct Node and Edge counts."""
    expected_result = f"IGRAPH U--- {EXPECTED_NODE_COUNT} {EXPECTED_EDGE_COUNT}"
    er_graph = build_er_graph(node_count, edge_count)
    assert expected_result in str(er_graph)
