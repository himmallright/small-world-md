from small_world_md.calc import *
from constants import *


def test_node_count(test_graph):
    assert node_count(test_graph) == EXPECTED_NODE_COUNT


def test_edge_count(test_graph):
    assert edge_count(test_graph) == EXPECTED_EDGE_COUNT


def test_avg_pl(test_graph):
    assert avg_pl(test_graph) == EXPECTED_AVG_PL


def test_avg_local_cc(test_graph):
    assert avg_local_cc(test_graph) == EXPECTED_AVG_LOCAL_CC


def test_sws(test_graph, test_rand_graph):
    assert sws(test_graph, supplied_rand_graph=test_rand_graph) == EXPECTED_SWS


def test_run_calcs(test_graph, test_rand_graph):
    """Tests the run_calc fn, with default param"""
    expected = {
        "node count": EXPECTED_NODE_COUNT,
        "edge count": EXPECTED_EDGE_COUNT,
        "average path length": EXPECTED_AVG_PL,
        "avg local clustering coefficient": EXPECTED_AVG_LOCAL_CC,
        "small world measure": EXPECTED_SWS,
    }
    assert run_calcs(test_graph, supplied_rand_graph=test_rand_graph) == expected


def test_run_calcs_just_node_count(test_graph):
    """Tests the run_calc fn, with only node count for run_list"""
    expected = {"node count": EXPECTED_NODE_COUNT}
    assert run_calcs(test_graph, run_list=["node count"]) == expected
