Areas: [[node a]]
Type: #parent
Associations: [[node a]], [[node-b]], [[node_c]], [[node d]], [[node F]] 
Tags: #tag1

---

The biggest node, that should link to everything other than the orphan node, including itself.