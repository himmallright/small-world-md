def compare_lists(list_a, list_b):
    list_a.sort()
    list_b.sort()
    if " ".join(list_a) == " ".join(list_b):
        return True
    else:
        return False


def compare_dicts(dict_a, dict_b):
    assert len(dict_a) == len(dict_b)
    for key in dict_a:
        assert dict_a[key] == dict_b[key]
