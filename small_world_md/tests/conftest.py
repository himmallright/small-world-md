import pytest

from small_world_md.graph import build_graph
from constants import TEST_MD_LINKS_DATA, TEST_RAND_GRAPH_DATA

# Note: This fixture assumes build_graph works!
@pytest.fixture()
def test_graph():
    """A Fixture to create and return a test network graph."""
    return build_graph(TEST_MD_LINKS_DATA)


@pytest.fixture()
def test_rand_graph():
    """A Fixture to create and return a test 'rand' network graph."""
    return build_graph(TEST_RAND_GRAPH_DATA)
