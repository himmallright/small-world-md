from pkg_resources import resource_filename
from small_world_md.files import *
from utils import compare_lists, compare_dicts

import pytest


def test_get_all_files():
    test_dir = resource_filename("small_world_md", "tests/test-vault/")

    expected_result = [
        "./dir2/node d.md",
        "./dir2/node-b.md",
        "./.obsidian/templates.json",
        "./tests/test-vault/.obsidian/config",
        "./tests/test-vault/.obsidian/workspace",
        "./node a.md",
        "./dir1/node_c.md",
        "./dir1/dir3/node e (template).md",
    ]

    returned_result = get_all_files(test_dir)
    assert returned_result.sort() == expected_result.sort()


def test_all_markdown_files():
    test_dir = resource_filename("small_world_md", "tests/test-vault/")

    file_list = [
        "./dir2/node d.md",
        "./dir2/node-b.md",
        "./.obsidian/templates.json",
        "./tests/test-vault/.obsidian/config",
        "./tests/test-vault/.obsidian/workspace",
        "./node a.md",
        "./dir1/node_c.md",
        "./dir1/dir3/node e (template).md",
    ]

    expected_result = [
        "./dir2/node d.md",
        "./dir2/node-b.md",
        "./node a.md",
        "./dir1/node_c.md",
        "./dir1/dir3/node e (template).md",
    ]

    returned_result = all_markdown_files(file_list)
    assert returned_result.sort() == expected_result.sort()


def test_get_all_links():
    file_list = [
        resource_filename("small_world_md", "tests/test-vault/dir2/node d.md"),
        resource_filename("small_world_md", "tests/test-vault/dir2/node-b.md"),
        resource_filename("small_world_md", "tests/test-vault/node a.md"),
        resource_filename("small_world_md", "tests/test-vault/dir1/node_c.md"),
        resource_filename(
            "small_world_md", "tests/test-vault/dir1/dir3/node e (template).md"
        ),
    ]
    expected_result = {
        "node d": ["node-b"],
        "node-b": ["node_c"],
        "node e (template)": [],
        "node_c": ["node d"],
        "node a": ["node a", "node_c", "node F", "node-b", "node d"],
    }
    returned_result = get_all_links(file_list)
    assert len(returned_result) == len(expected_result)
    for key in expected_result:
        assert compare_lists(returned_result[key], expected_result[key])


def test_grab_internal_links():
    test_content = "Areas: [[node a]]\
Type: #parent\
Associations: [[node a]], [[node-b]], [[node_c]], [[node d]], [[node F]] \
Tags: #tag1\
\
---\
\
The biggest node, that should link to everything other than the orphan node, including itself."

    expected_result = ["node a", "node-b", "node_c", "node d", "node F"]
    returned_result = grab_internal_links(test_content)
    # import pdb; pdb.set_trace()
    # assert " ".join(returned_result) == " ".join(expected_result)
    assert compare_lists(returned_result, expected_result)


def test_parse_md_file():
    test_file = resource_filename("small_world_md", "tests/test-vault/node a.md")

    expected_name = "node a"
    expected_links = ["node a", "node_c", "node F", "node-b", "node d"]
    result_name, result_links = parse_md_file(test_file)
    assert result_name == expected_name
    assert compare_lists(result_links, expected_links)


def test_process_dir():
    test_dir = resource_filename("small_world_md", "tests/test-vault/")

    expected_result = {
        "node d": ["node-b"],
        "node-b": ["node_c"],
        "node e (template)": [],
        "node_c": ["node d"],
        "node a": ["node a", "node_c", "node F", "node-b", "node d"],
    }
    returned_result = process_dir(test_dir)
    assert len(returned_result) == len(expected_result)
    for key in expected_result:
        assert compare_lists(returned_result[key], expected_result[key])
