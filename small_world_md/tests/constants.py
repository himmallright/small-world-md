TEST_MD_LINKS_DATA = {
    "node d": ["node-b"],
    "node-b": ["node_c"],
    "node e (template)": [],
    "node_c": ["node d"],
    "node a": ["node a", "node_c", "node F", "node-b", "node d"],
}

TEST_RAND_GRAPH_DATA = {
    "node 0": ["node 2", "node 5"],
    "node 1": ["node 4"],
    "node 2": [],
    "node 3": ["node 2", "node 5"],
    "node 4": ["node 2", "node 3"],
    "node 5": ["node 4"],
}

EXPECTED_NODE_COUNT = 6
EXPECTED_EDGE_COUNT = 8

EXPECTED_AVG_PL = 1.3
EXPECTED_AVG_LOCAL_CC = 0.875

EXPECTED_SWS = 3.0961538461538461
