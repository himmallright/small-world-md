from argparse import ArgumentParser
from small_world_md.calc import run_calcs
from small_world_md.files import process_dir
from small_world_md.graph import build_graph


class CLI:
    """the main CLI object. It matches the cli imputs and kick off the
    appropriate functionality."""

    def __init__(self):
        self.parser = ArgumentParser(
            description="Read in Obsidian Vault data\
        and run network calculations on it"
        )
        self.parser.add_argument(
            "vault_src", type=str, help="The location of the obsidian vault to import."
        )
        self.args = vars(self.parser.parse_args())

    def main(self):
        file_data = process_dir(self.args["vault_src"])
        vault_graph = build_graph(file_data)
        calc_results = run_calcs(vault_graph)  # TODO: Add arg results here
        self.print_calcs(calcs=calc_results)

    def print_calcs(self, calcs={}):
        """Prints out the calculations dictionary provided."""
        print("Vault Graph Calculations:")
        if calcs:
            for key in calcs:
                print(f"\t{key}: {calcs[key]}")
        else:
            print("No calculations completed")
