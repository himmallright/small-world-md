from igraph import *


def build_graph(md_links_data):
    """Loads the parsing data structure into a igraph Graph obj."""

    node_index_dict = create_node_index(md_links_data)
    g = Graph()
    g.add_vertices(len(node_index_dict))

    for file_node_name in md_links_data:
        file_node_id = node_index_dict[file_node_name]
        file_node_links = md_links_data[file_node_name]
        node_link_ids = [node_index_dict[link_name] for link_name in file_node_links]
        edges_to_add = []
        for link_id in node_link_ids:
            edges_to_add.append((file_node_id, link_id))
        g.add_edges(edges_to_add)
    return g


def build_er_graph(nodes, edges):
    """Generates and returns an Erdos-Renyi Graph"""
    return Graph.Erdos_Renyi(n=nodes, m=edges)


def create_node_index(md_links_data):
    """Takes the links dictionary, and returns a numbered index of the items."""
    node_index_dict = {}
    index = 0
    all_nodes = get_all_nodes_set(md_links_data)
    all_nodes.sort()
    for node_name in all_nodes:
        node_index_dict[node_name] = index
        index += 1
    return node_index_dict


def get_all_nodes_set(md_links_data):
    """Collects and returns a set list of all the nodes in the links data.
    The include files from nodes, as well as nodes that exist only as a link
    with no file associated with them."""
    all_nodes = []
    for file_node in md_links_data:
        link_nodes = md_links_data[file_node]
        all_nodes.append(file_node)
        all_nodes += link_nodes
    all_nodes = list(set(all_nodes))
    all_nodes.sort()
    return all_nodes
