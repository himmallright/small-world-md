# from igraph import *
from small_world_md.graph import build_er_graph

VALID_CALC_ITEMS = [
    "node count",
    "edge count",
    "average path length",
    "avg local clustering coefficient",
    "small world measure",
]


def run_calcs(vault_graph, run_list=VALID_CALC_ITEMS, supplied_rand_graph=None):
    """Run a set of calculations, and return the results."""
    # Check run items are valid
    assert set(run_list).issubset(set(VALID_CALC_ITEMS))
    run_calcs = {}

    if "node count" in run_list:
        run_calcs["node count"] = node_count(vault_graph)
    if "edge count" in run_list:
        run_calcs["edge count"] = edge_count(vault_graph)
    if "average path length" in run_list:
        run_calcs["average path length"] = avg_pl(vault_graph)
    if "avg local clustering coefficient" in run_list:
        run_calcs["avg local clustering coefficient"] = avg_local_cc(vault_graph)
    if "small world measure" in run_list:
        run_calcs["small world measure"] = sws(
            vault_graph, supplied_rand_graph=supplied_rand_graph
        )
    return run_calcs


def node_count(vault_graph):
    """The node count of the graph."""
    return vault_graph.vcount()


def edge_count(vault_graph):
    """The edge count of the graph."""
    return vault_graph.ecount()


def avg_pl(vault_graph):
    """The average path length of the graph."""
    return vault_graph.average_path_length()


def avg_local_cc(vault_graph):
    """The local cluster coefficient of the graph."""
    return vault_graph.transitivity_avglocal_undirected()


def sws(vault_graph, supplied_rand_graph=None):
    """The basic Small World Measure calculation."""
    if supplied_rand_graph:
        rand_graph = supplied_rand_graph
    else:
        rand_graph = build_er_graph(node_count(vault_graph), edge_count(vault_graph))
    cc_ratio = avg_local_cc(vault_graph) / avg_local_cc(rand_graph)
    apl_ratio = avg_pl(vault_graph) / avg_pl(rand_graph)
    sw = cc_ratio / apl_ratio
    return sw
