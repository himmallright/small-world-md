from os import path, walk
from pathlib import Path

import re


def process_dir(root_dir="."):
    """Recursively grabs all the markdown links from a dir, parses them, and
    returns a dictionary of each item as the key with a list of all it's links
    for the vale."""
    all_files = get_all_files(root_dir=root_dir)
    md_files = all_markdown_files(all_files)
    return get_all_links(md_files)


def get_all_links(md_file_list):
    """Parses all the md files, returns a datastructure of the links."""
    all_links = {}
    for md_file in md_file_list:
        name, internal_links = parse_md_file(md_file)
        all_links[name] = internal_links
    return all_links


def parse_md_file(md_file_src):
    """Parses the md files. Returns a list of the matched internal links?"""
    name = Path(md_file_src).stem
    with open(md_file_src, "r") as md_file:
        content = md_file.read()
        internal_links = grab_internal_links(content)
    return name, internal_links


def grab_internal_links(content, match_string="\[\[(.*?)\]\]"):
    """Parses the content from a markdown file and returns a list
    of all the internal links."""
    r = re.compile(match_string)
    return list(set(r.findall(content)))


def all_markdown_files(file_list, match_string=".md$"):
    """Returns all markdown files from a list of file paths."""
    return [file_src for file_src in file_list if re.search(match_string, file_src)]


def get_all_files(root_dir="."):
    """Returns a list of all files from a directory."""
    all_files = []
    for root, dirs, files in walk(root_dir, topdown=False):
        for name in files:
            all_files.append(path.join(root, name))
    return all_files
